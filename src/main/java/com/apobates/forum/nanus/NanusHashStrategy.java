package com.apobates.forum.nanus;

/**
 * 存储内容的散列策略
 * @author xiaofanku@live.cn
 * @since 20210827
 */
public interface NanusHashStrategy {
    /**
     * 编码
     * @param value 编码目标
     * @param defaultValue 编码失败时的默认值
     * @return
     */
    String encode(String value, String defaultValue);

    /**
     * 解码
     * @param value 解码目标
     * @param defaultValue 解码失败时的默认值
     * @return
     */
    String decode(String value, String defaultValue);
}
