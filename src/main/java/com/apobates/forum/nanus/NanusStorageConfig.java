package com.apobates.forum.nanus;

import org.apache.commons.lang3.StringUtils;

/**
 * 存储配置
 * @author xiaofanku@live.cn
 * @since 20210827
 */
public class NanusStorageConfig {
    //域名,需要带有http[s]
    private final String domain;
    //路径
    private final String path;
    //有效期单位
    private final NanusUnitEnum unit;
    //有效期
    private final int duration;
    //key
    private final String keyName;

    private NanusStorageConfig(String domain, String path, NanusUnitEnum unit, int duration, String keyName) {
        this.domain = domain;
        this.path = path;
        this.unit = unit;
        this.duration = duration;
        this.keyName = keyName;
    }

    private NanusStorageConfig(Builder builder) {
        this.domain = builder.domain;
        this.path = builder.path;
        this.unit = builder.unit;
        this.duration = builder.duration;
        this.keyName = builder.keyName;
    }

    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    public NanusUnitEnum getUnit() {
        return unit;
    }

    public int getDuration() {
        return duration;
    }

    public String getKeyName() {
        return keyName;
    }

    /**
     * site是否启用了HTTPS,是返回true
     *
     * @return
     */
    public boolean isHttps() {
        if(StringUtils.isNotBlank(getDomain())) {
            return getDomain().startsWith("https");
        }
        return false;
    }

    /**
     * 返回一个默认配置
     * @param domain 域名,需要包含协议部分(http|https)
     * @return
     */
    public static Builder defaultConfig(String domain){
        return new Builder(domain);
    }

    public static class Builder{
        //域名,需要带有http[s]
        private String domain;
        //路径
        private String path="/";
        //有效期单位
        private NanusUnitEnum unit= NanusUnitEnum.DAY;
        //有效期
        private int duration=1;
        //key
        private String keyName="nanus";

        private Builder(String domain) {
            this.domain = domain;
        }

        /**
         * 设置作用域路径.
         * @param path
         * @return
         */
        public Builder setPath(String path){
            this.path = path;
            return this;
        }

        /**
         * 设置过期单位
         * @param unit
         * @return
         */
        public Builder setExpiresUnit(NanusUnitEnum unit){
            this.unit = unit;
            return this;
        }

        /**
         * 设置过期时长
         * @param duration
         * @return
         */
        public Builder setExpiresduration(int duration){
            this.duration = duration;
            return this;
        }

        /**
         * 设置key
         * @param keyName
         * @return
         */
        public Builder setKeyName(String keyName){
            this.keyName = keyName;
            return this;
        }

        /**
         * 返回构造的存储配置
         * @return
         */
        public NanusStorageConfig build(){
            return new NanusStorageConfig(this);
        }
    }
}
