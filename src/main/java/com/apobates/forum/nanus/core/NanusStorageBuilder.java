package com.apobates.forum.nanus.core;

import com.apobates.forum.nanus.NanusConverter;
import com.apobates.forum.nanus.NanusHashStrategy;
import com.apobates.forum.nanus.NanusStorage;
import com.apobates.forum.nanus.NanusStorageConfig;
import com.apobates.forum.nanus.core.cookie.NanusCookieStorageProvider;
import com.apobates.forum.nanus.core.session.NanusSessionStorageProvider;

/**
 * 存储方案的构造器
 * @author xiaofanku@live.cn
 * @since 20210827
 */
public class NanusStorageBuilder {
    private final NanusConverter converter;
    private final NanusStorageConfig config ;
    private final NanusHashStrategy strategy;

    /**
     *
     * @param converter 存储对象转换器
     * @param config 存储配置
     * @param strategy 存储内容的散列策略
     */
    private NanusStorageBuilder(NanusConverter converter, NanusStorageConfig config, NanusHashStrategy strategy) {
        this.converter = converter;
        this.config = config;
        this.strategy = strategy;
    }

    /**
     * 设置存储对象转换器
     * @param converter
     * @return
     */
    public NanusStorageBuilder convert(NanusConverter converter){
        return new NanusStorageBuilder(converter, getConfig(), getStrategy());
    }

    /**
     * 设置存储配置
     * @param config
     * @return
     */
    public NanusStorageBuilder config(NanusStorageConfig config){
        return new NanusStorageBuilder(getConverter(), config, getStrategy());
    }

    /**
     * 设置存储内容的散列策略
     * @param strategy
     * @return
     */
    public NanusStorageBuilder strategy(NanusHashStrategy strategy){
        return new NanusStorageBuilder(getConverter(), getConfig(), strategy);
    }

    /**
     * 返回无默认设置的存储配置
     * @return
     */
    public static NanusStorageBuilder getInstance(){
        return new NanusStorageBuilder(
                null,
                null,
                null);
    }

    /**
     * 返回使用默认存储配置
     * 默认使用：
     * NanusHashStrategy: Base64
     * NanusStorageConfig: 有效期(1天),路径(/),key(nanus)
     * @param configDomain NanusStorageConfig.domain
     * @return
     */
    public static NanusStorageBuilder getBase64Instance(String configDomain){
        return new NanusStorageBuilder(
                null,
                NanusStorageConfig.defaultConfig(configDomain).build(),
                new NanusBASE64Strategy());
    }

    /**
     * 返回使用默认存储配置
     * 默认使用：
     * NanusHashStrategy: DES
     * NanusStorageConfig: 有效期(1天),路径(/),key(nanus)
     * @param configDomain NanusStorageConfig.domain
     * @param strategyDESSecret DES的公钥
     * @return
     */
    public static NanusStorageBuilder getDESInstance(String configDomain, String strategyDESSecret){
        return new NanusStorageBuilder(
                null,
                NanusStorageConfig.defaultConfig(configDomain).build(),
                new NanusDesStrategy(strategyDESSecret));
    }

    public NanusConverter getConverter() {
        return converter;
    }

    public NanusStorageConfig getConfig() {
        return config;
    }

    public NanusHashStrategy getStrategy() {
        return strategy;
    }

    /**
     * 返回存储方案
     * @param type 支持:cookie,session
     * @return
     * @throws UnsupportedOperationException
     */
    public NanusStorage build(String type)throws UnsupportedOperationException{
        if("cookie".equalsIgnoreCase(type)){
            return new NanusCookieStorageProvider(converter, config, strategy);
        }

        if("session".equalsIgnoreCase(type)){
            return new NanusSessionStorageProvider(converter, config, strategy);
        }
        throw new UnsupportedOperationException("系统目前只支持:cookie或session");
    }
}
