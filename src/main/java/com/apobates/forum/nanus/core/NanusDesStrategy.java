package com.apobates.forum.nanus.core;

import com.apobates.forum.nanus.NanusHashStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 使用DES作散列策略
 * @author xiaofanku@live.cn
 * @since 20210827
 */
public class NanusDesStrategy implements NanusHashStrategy {
    private final String secret;
    private final static Logger logger = LoggerFactory.getLogger(NanusDesStrategy.class);

    public NanusDesStrategy(String secret) {
        this.secret = secret;
    }

    @Override
    public String encode(String value, String defaultValue) {
        try {
            NanusDesCryp mdc = new NanusDesCryp(secret);
            return mdc.encrypt(value);
        }catch (Exception e){
            if (logger.isDebugEnabled()){
                logger.debug(e.getMessage(), e);
            }
        }
        return defaultValue;
    }

    @Override
    public String decode(String value, String defaultValue) {
        try {
            NanusDesCryp mdc = new NanusDesCryp(secret);
            return mdc.decrypt(value);
        }catch (Exception e){
            if (logger.isDebugEnabled()){
                logger.debug(e.getMessage(), e);
            }
        }
        return defaultValue;
    }
}
