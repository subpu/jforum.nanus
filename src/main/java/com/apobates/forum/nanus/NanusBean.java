package com.apobates.forum.nanus;

import java.io.Serializable;

/**
 * 保存的对象
 * @author xiaofanku@live.cn
 * @since 20210827
 */
public interface NanusBean extends Serializable {
    /**
     * uuid/用户ID
     * @return
     */
    String uuid();

    /**
     * names/用户名
     * @return
     */
    String names();
}
