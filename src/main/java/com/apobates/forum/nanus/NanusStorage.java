package com.apobates.forum.nanus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.Optional;

/**
 * 存储方案
 * @author xiaofanku@live.cn
 * @since 20210827
 */
public interface NanusStorage extends Serializable {
    /**
     * 保存
     *
     * @param bean 存储对象
     * @param request
     * @param response
     */
    void store(NanusBean bean, HttpServletRequest request, HttpServletResponse response);

    /**
     * 销毁
     *
     * @param request
     * @param response
     */
    void delete(HttpServletRequest request, HttpServletResponse response);

    /**
     * 返回存储的对象
     *
     * @param request
     * @return
     */
    Optional<NanusBean> getInstance(HttpServletRequest request);

    /**
     * 刷新
     *
     * @param request
     * @param response
     * @param newBean 新的存储对象
     */
    void refresh(HttpServletRequest request, HttpServletResponse response, NanusBean newBean);

    /**
     * 是否支持还原功能
     *
     * @return
     */
    boolean isSupportRevival();
}
