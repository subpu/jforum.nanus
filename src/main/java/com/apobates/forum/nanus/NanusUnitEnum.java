package com.apobates.forum.nanus;

import java.time.LocalDateTime;

/**
 * 时间单位枚举
 * @author xiaofanku@live.cn
 * @since 20210827
 */
public enum NanusUnitEnum {
    MINUTE(1, "分钟") {
        @Override
        protected LocalDateTime plug(int duration, LocalDateTime startDateTime) {
            return startDateTime.plusMinutes(duration);
        }
    },
    HOUR(2, "小时") {
        @Override
        protected LocalDateTime plug(int duration, LocalDateTime startDateTime) {
            return startDateTime.plusHours(duration);
        }
    },
    DAY(3, "天") {
        @Override
        protected LocalDateTime plug(int duration, LocalDateTime startDateTime) {
            return startDateTime.plusDays(duration);
        }
    },
    WEEK(4, "周") {
        @Override
        protected LocalDateTime plug(int duration, LocalDateTime startDateTime) {
            return startDateTime.plusWeeks(duration);
        }
    },
    MONTH(5, "月") {
        @Override
        protected LocalDateTime plug(int duration, LocalDateTime startDateTime) {
            return startDateTime.plusMonths(duration);
        }
    },
    YEAR(6, "年") {
        @Override
        protected LocalDateTime plug(int duration, LocalDateTime startDateTime) {
            return startDateTime.plusYears(duration);
        }
    };

    private final String title;
    private final int symbol;

    private NanusUnitEnum(int symbol, String title) {
        this.title = title;
        this.symbol = symbol;
    }

    public String getTitle() {
        return title;
    }

    public int getSymbol() {
        return symbol;
    }
    //
    public LocalDateTime plusDateTime(int number, LocalDateTime start) {
        int calcStep = (number <= 0) ? 1 : number;
        LocalDateTime startDateTime = (start == null) ? LocalDateTime.now() : start;
        //
        return this.plug(calcStep, startDateTime);
    }

    protected abstract LocalDateTime plug(int calcStep, LocalDateTime startDateTime);
}
