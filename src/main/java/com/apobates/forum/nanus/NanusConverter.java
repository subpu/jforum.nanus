package com.apobates.forum.nanus;

import java.util.Map;

/**
 * 存储对象转换器
 * @author xiaofanku@live.cn
 * @since 20210827
 */
public interface NanusConverter {
    /**
     * 将存储对象转为map
     * @param bean
     * @return
     */
    Map<String,String> toMap(NanusBean bean);

    /**
     * 将Map转为存储对象
     * @param map
     * @return
     */
    NanusBean toBean(Map<String,String> map);
}
